/*---------------------------------------------------------------
  :: Capsule CRM
  -> adapter

  
---------------------------------------------------------------*/

var Errors = require('waterline-errors').adapter;
var _runJoins = require('waterline-cursor');
var Capsule = require('capsule-crm');

module.exports = (function () {

  // Hold connections for this adapter
  var connections = {};

  var adapter = {

    identity: 'sails-capsulecrm',

    // Which type of primary key is used by default
    pkFormat: 'integer',
    autoPK: false,
    autoCreateAt: false,
    autoUpdatedAt: false,

    // Whether this adapter is syncable (yes)
    syncable: false,

    // How this adapter should be synced
    migrate: 'safe',

    // Allow a schemaless datastore
    defaults: {
      schema: false
    },

    // Register A Connection
    registerConnection: function (connection, collections, cb) {
		console.log("CapsuleCRM connection registered for:",connection.identity);
		if(!connection.identity) return cb(Errors.IdentityMissing);
		if(connections[connection.identity]) return cb(Errors.IdentityDuplicate);
		
		connections[connection.identity] = {connection: connection, collections: collections}
		
		// initialize capsule connection
		connections[connection.identity].capsule = Capsule.createConnection(connection.capsuleUser, connection.capsuleAPIKey);
		cb();
    },


    teardown: function (conn, cb) {

      if (typeof conn == 'function') {
        cb = conn;
        conn = null;
      }
      if (conn == null) {
        connections = {};
        return cb();
      }
      if(!connections[conn]) return cb();
      delete connections[conn];
      cb();
    },

    // Return attributes
    describe: function (conn, coll, cb) {
	    console.log("CRM - describe called");
	    cb();
      //grabConnection(conn).describe(coll, cb);
      //cb(null,{id: {type: "varchar", primaryKey: true}, contacts: {type: "varchar"}, pictureURL: {type: "varchar"}, createdOn: {type: "datetime"}, updatedOn: {type: "datetime"}, firstName: {type: "varchar"}, lastName: {type: "varchar"}});
    },

    define: function (conn, coll, definition, cb) {
	    console.log("CRM - define called");
	    cb();
      //grabConnection(conn).createCollection(coll, definition, cb);
    },

    drop: function (conn, coll, relations, cb) {
	    console.log("CRM - drop called");
	    cb();
      //grabConnection(conn).dropCollection(coll, relations, cb);
    },

    join: function (conn, coll, criteria, cb) {
		console.log("CRM - join called", JSON.stringify(criteria));
		
		var connection = grabConnection(conn);
		
		var parentIdentity = coll;
		
		// Populate associated records for each parent result
		// (or do them all at once as an optimization, if possible)
		_runJoins({
		
		instructions: criteria,
		parentCollection: parentIdentity,
		
		/**
		 * Find some records directly (using only this adapter)
		 * from the specified collection.
		 *
		 * @param  {String}   collectionIdentity
		 * @param  {Object}   criteria
		 * @param  {Function} cb
		 */
		$find: function (collectionIdentity, criteria, cb) {
		    console.log("$FIND:",collectionIdentity, criteria);
		    
		    connection.capsule.request({path: "/"+collectionIdentity+"/"+criteria.where.id},function(err,data){
		     
			    if(err){
					return cb(null,null)
				} else {
					if (collectionIdentity=="party"){
						return cb(null,data.person);
					} else {
						return cb(null,null);
					}
				}
		        
		    })
		    
		},
		
		/**
		 * Look up the name of the primary key field
		 * for the collection with the specified identity.
		 *
		 * @param  {String}   collectionIdentity
		 * @return {String}
		 */
		$getPK: function (collectionIdentity) {
		  if (!collectionIdentity) return;
		  return "id";
		}
		}, cb);
		
		
		//
    },

    find: function (conn, coll, options, cb) {
	  
	    
	    
	  
	  var connection = grabConnection(conn);
      console.log(options);
      var iterationArray = []; 
      var Query = "";
      var Results = [];
          	
      // variations of filter criteria depending on collection (coll)
      
      if (options.where || options.where["id"]) {
	      if (Array.isArray(options.where["id"])) {
	      	iterationArray = options.where["id"];
		  } else {
			  iterationArray = false ;
		  }
	      
      } else { iterationArray = false } 
      
            
      console.log("iterations:" , iterationArray);
      
      async.each(iterationArray, function(item,_cb) {
	  
	  if (Array.isArray(iterationArray)) {
	      options.where["id"] = item;
	      
      }   
	     
	  	Query = _createQuery(coll,options,cb);
		
		Query.method = "GET";
						
		console.log("Query:",Query)
		connection.capsule.request(Query,function(err,data){
			console.log(err,data);
			if(err){
				cb(null,err)
			} else {
				//console.log(data);
				switch(coll) {
			    case 'person':
			        if(data.parties){
							//cb(null,data.parties.person);
							Results.push(data.parties.person)
							_cb();
						} else {
							//cb(null,data.person);
							Results.push(data.person)
							_cb();
						}
			        break;
			        
			    case 'organization':
			        if(data.parties){
							//cb(null,data.parties.organization);
							Results.push(data.parties.organization)
							_cb();
						} else {
							//cb(null,data.organization);
							Results.push(data.organization);
							_cb();
						}
			        break;
			        
			    case 'tag':
			    	cb(null,data.tags.tag);
			        break;
			        
			    default:
		        	return cb(Errors.NotFound);
		        
				}
				
			}
				
			
	    });   
	     
	     
	     
	     
	     
	     
	      
      }, function(err){
	      if ( err ) { cb(null,err); }
	      else {
		      console.log("RESULTS: ", Results);
		      cb(null,Results);
	      }
	      
      })
      
      
    },

    create: function (conn, coll, values, cb) {
      console.log("CRM - create called");
      console.log("coll:",JSON.stringify(coll),"values:",JSON.stringify(values)) ;
      var Query = {};
      var connection = grabConnection(conn);
      
      Query.method = "POST";
      
      switch(coll) {
	    case 'person':
	    	Query.data = {person: values};
	        Query.path = "/person";
	        console.log(Query);
				connection.capsule.request(Query,function(err,data, response){ 
					if ( err ) return cb( err );
					// query capsule for newly created party based on id returned in response.
					connection.capsule.request({path: "/party/"+response.headers.location.split("/")[5]}, function(err,data){
						if ( err ) cb( err ) 
				     	else cb(false, data.person); 
		    		})
		    	});
			break;
			
		case 'organization':
			Query.data = {organization: values};
	        Query.path = "/organization";
				connection.capsule.request(Query,function(err,data, response){ 
					if ( err ) return cb( err );					
			      	// query capsule for newly created party based on id returned in response.
				  	connection.capsule.request({path: "/party/"+response.headers.location.split("/")[5]}, function(err,data){
						if ( err ) cb( err )
				     	else cb(false, data.organization); 				  		
		    		})
		    	})
			break;
			
	    case 'tag':
	    	return cb(Errors.CollectionNotRegistered);
	        break;
	        
	    default:
        	return cb(Errors.NotFound);
        
		}

    },

    update: function (conn, coll, options, values, cb) {
	    console.log("CRM - update called");
      	console.log("coll:",JSON.stringify(coll),"values:",JSON.stringify(values)) ;
		
		var Query = {};
		var connection = grabConnection(conn);
		var capsuleid;
		
		require('async').series([
			function(_cb){
				// first we need to find the appropriate record.
				// if we have the id we are gold
				if (options.where["id"]){
					capsuleid = options.where["id"];
					_cb(); // next step (update)
				} else {
					// if not we need to query and get the id
					Query = _createQuery(coll,options,cb);
					Query.method = "GET";
					connection.capsule.request(Query,function(err,data){
						if(err){
							return cb(Error.NotFound);
						} else {
							switch(coll) {
						    case 'person':
						        if(data.parties){
							        	// if multiple return first - TODO see if we can update multiple records
										capsuleid = data.parties.person[0].id;
									} else {
										capsuleid = data.person.id;
									}
						        break;
						        
						    case 'organization':
						        if(data.parties){
										capsuleid = data.parties.organization[0].id;
									} else {
										capsuleid = data.organization.id;
									}
						        break;
						        
						    				        
						    default:
					        	return cb(Errors.NotFound);
					        
							}
						}
					// we now have capsuleid
					// move on to next step (update)
					_cb();
					})
				}
			},
			
			function(_cb){
				// update
				Query = {};
				Query.method = "PUT";
				switch(coll) {
				case 'person':
					Query.data = {person: values};
					Query.path = "/person/"+capsuleid;
				    console.log(Query);
					connection.capsule.request(Query,function(err,data){
						console.log(err,data); 
						_cb(err);
			    		
			    	});
					break;
					
				case 'organization':
					Query.data = {organization: values};
				    Query.path = "/organization";
					connection.capsule.request(Query,function(err,data){
						console.log(err,data); 
						_cb(err);
			    	});
					break;
					
				case 'tag':
					return _cb(Errors.CollectionNotRegistered);
				    break;
				    
				default:
					return _cb(Errors.NotFound);
				
				} 
				
			},
			function(_cb){
				// retreive CRM updated values
				
				connection.capsule.request({path: "/party/"+capsuleid}, function(err,results){
					console.log("new capsule:",results);
					switch(coll) {
					case 'person':
					 	cb(err,results.person);
					 	break;
					 case 'organization':
					 	cb(err,results.organization);
					 	break;
					 default:
					 	cb(Error.NotFound);
					}
				})
			}
			
		], function(err){
			cb(Error.NotFound);
		})
		
    },

    destroy: function (conn, coll, options, cb) {
	    console.log("CRM - destroy called - not yet implemented");
	    cb();
      //grabConnection(conn).destroy(coll, options, cb);
    }

  };

  /**
   * Grab the connection object for a connection name
   *
   * @param {String} connectionName
   * @return {Object}
   * @api private
   */

  function grabConnection(connectionName) {
    return connections[connectionName];
  }
  
  
  function _createQuery(coll,options,cb) {
	  console.log("_createQuery:: coll:",coll, "options",options);
	    var Query = {search: ""};  
	     switch(coll) {
		    case 'person':
		    	console.log("CRM - person, party collection");
		    	
		        // build Query search string
		      	for (var key in options.where){
					// key has to be one of the following to be usable.
					if(key == "q" || key == "email" || key == "tag" || key == "lastmodified") {
						Query.search = Query.search + "&"+key+"="+options.where[key];
					} 
				}
				
				// build Query path
				if(options.where){
					if(options.where["id"]){
						Query.path = "/party/"+options.where["id"];
						// we are looking for a specific party remove search definition.
						delete Query.search;
						
					} else {
						Query.path = "/party";
					}
				} else {
					return cb(Errors.NotFound);
				}
				
		        break;
		        
		    case 'organization':
		    	console.log("CRM - organization, party collection");
		    	Query.path = "/party";
		    	
		        // build Query search string
		      	for (var key in options.where){
					// key has to be one of the following to be usable.
					if(key == "q" || key == "email" || key == "tag" || key == "lastmodified") {
						Query.search = Query.search + "&"+key+"="+options.where[key];
					} 
				}
				
				// build Query path
				if(options.where){
					if(options.where["id"]){
						Query.path = "/party/"+options.where["id"];
						// we are looking for a specific party remove search definition.
						delete Query.search;
						
					} else {
						Query.path = "/party";
					}
				} else {
					return cb(Errors.NotFound);
				}
				
		        break;
		    case 'tag':
		    	console.log("CRM - tag collection");
		        if(options.where){
					// The where.id has to be available
					if(options.where.id && options.where.resource){
						// set Query path
						Query.path = "/"+options.where.resource+"/"+options.where["id"]+"/tag";
						break;
					}
					
				}
				return cb(Errors.NotFound);
		        break;
		        
		    default:
		        return cb(Errors.NotFound);
		        
		}
      	
		if(Query.search){
			// strip of the initial &
			Query.search = Query.search.substring(1);
		} else {
			//delete search attribute if it is empty
			delete Query.search;
		} 
		
		return Query;
	      
    }
  
  

  return adapter;
})();
