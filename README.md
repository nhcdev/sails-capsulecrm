![image_squidhome@2x.png](http://i.imgur.com/RIvu9.png) 

# sails-capsulecrm

## Sailsjs/Waterline adapter for Capsule CRM (Beta v0.10.12)

This adapter connects to the Capsule CRM cloud service and allows sails/waterline ORM to access and modify this database.  This adapter is currently in development and not all capabilities or datatypes are supported.  Development is performed as and when capabilities are required in one of our other prodjects.  Please feel free to fork and sumbit pull requests for further development.  Issue tracking and feature requests can be done through bitbucket repository.

/config/connections.js required attributes:

	module.exports.connections = {
		myConnection: {
			module: "sails-capsulecrm"
			capsuleUser: [your capsule crm hostname]
			capsuleAPIKey: [your capsule crm API key]
		}
	}

Capsule CRM hostname in your Capsule CRM URL (https://[hostname].capsulecrm.com)

sails-capsulecrm does not support autoPK, autoCreatedAt, autoUpdatedAt, these are handled by the capsule-crm service, so they are set to false in the adapter by default and shouldn't be overridden in the connections or model definitions.  also migrate: 'safe' is the default as the capsule database can't be altered or dropped, do not override this setting as it will just add time to your server startup, but it will not effect the data in your capsule account.

When setting up the model definition use top-level tableName: attribute to define the resource type (person, organization, etc.)  (see CapsuleCRM API for details)

/api/models/MyModel.js

	module.exports = {
		autoCreatedAt: false,
		autoUpdatedAt: false,
		autoPK: false,
		migrate: "safe",
		connection: "myConnection",
		tableName: "person",
		attributes: {
			id: {type: "string", primaryKey: true},   //required
			firstName: "string",		// all other fields are optional
			lastName: "string",
			contacts: "object",
			...
			  		
	  }
	};

## Current capabilities include:

as of 0.10.6

	find(), findOne(), findOneById() functions for parties (person and organization) has been implemented.

	create() function for parties (person and organization) has been implemented.

	update() function for parties (person and organization) has been implemented.

as of 0.10.12

	associations: added support for association joins for one or multiple parties. (not fully tested)



## About Sails.js
http://sailsjs.com

## About Waterline
Waterline is a new kind of storage and retrieval engine.  It provides a uniform API for accessing stuff from different kinds of databases, protocols, and 3rd party APIs.  That means you write the same code to get users, whether they live in mySQL, LDAP, MongoDB, or Facebook.
